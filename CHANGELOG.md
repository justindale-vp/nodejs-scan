# NodeJsScan analyzer changelog

## v2.7.0
- Switch to the MIT Expat license (!60)

## v2.6.0
- Add `SAST_DISABLE_BABEL` flag (!58)

## v2.5.0
- Update logging to be standardized across analyzers (!59)

## v2.4.1
- Remove `location.dependency` from the generated SAST report (!56)

## v2.4.0
- Update third-party dependencies to latest versions (!40)

## v2.3.0
- Add `id` field to vulnerabilities in JSON report (!31)

## v2.2.0
- Add support for custom CA certs (!27)

## v2.1.1
- Allow babel to run from anywhere in the FS, to fix a bug in non-DinD mode (!19)

## v2.1.0
- Upgrade Babel from 6 to 7 (!20)

## v2.0.2
- Set full default path for `rules.xml` to avoid error when running without Docker-in-Docker (!17)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add an `Identifier` generated from the NodeJsScan's rule name

## v1.1.0
- Add `Scanner` property and deprecate `Tool`

## v1.0.0
- Initial release
